#!usr/bin/ruby

r = [10,20,50,100,200,500,1000]

r.each{|rm|
  pr = (10**3) / ((rm)**4.0)
  p pr
  print("r = #{rm} 10logpr = #{10*Math.log10(pr)}\n")
}
